from typing import Optional
from pydantic import BaseModel
from fastapi import FastAPI

app = FastAPI()


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/items/{item_id}")
def read_item(item_id: int, q: Optional[str] = None):
    return {"item_id": item_id, "q": q}


class Mot():
    """
    id: int
    caracteres: str
    """
    def __init__(self, id, caracteres):
        self.id = id
        self.caracteres = caracteres


mots = [Mot(0, "alice"), Mot(1, "bob")]


@app.get("/mots")
def get_mots(q: Optional[str] = None):
    return mots


@app.post("/mot/{mot}")
def post_mot(mot):
    pass
